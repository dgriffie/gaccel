unit Driver;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Math, Windows, Transfer, interception;

type
  TDriver = class(TThread)
    PointerResolution: double;
    PointerFrequency: double;
    DisplayResolution: double;
    Transfer: TTransfer;
    procedure Execute; override;
  end;

implementation

procedure TDriver.Execute();
var
  context: InterceptionContext;
  device: InterceptionDevice;
  mstroke: InterceptionMouseStroke;
  prevFrameTime, frameTime, freq: LARGE_INTEGER;
  dx, dy, t, v, g: double;
  cx: double = 0;
  cy: double = 0;
begin
  self.Priority:= tpTimeCritical;
  context := interception_create_context();

  interception_set_filter(context, @interception_is_mouse,
    INTERCEPTION_FILTER_MOUSE_MOVE);

  QueryPerformanceFrequency(@freq);
  QueryPerformanceCounter(@prevFrameTime);

  while not CheckTerminated do
  begin
    device := interception_wait(context);
    if (device = 0) or (interception_receive(context, device, @mstroke, 1) < 0) then
    begin

      continue;
    end;

    if (mstroke.flags and INTERCEPTION_MOUSE_MOVE_ABSOLUTE) = 0 then
    begin
      QueryPerformanceCounter(@frameTime);
      t := (frameTime.QuadPart - prevFrameTime.QuadPart) / freq.QuadPart;
      if t > 1/PointerFrequency then
         t := 1/PointerFrequency;
      prevFrameTime := frameTime;
      t := 1/ PointerFrequency;
      dx := mstroke.x / PointerResolution;
      dy := mstroke.y / PointerResolution;
      v := sqrt(dx * dx + dy * dy) / t;

      g := Transfer.Gain(v);

      dx := dx * g * DisplayResolution + cx;
      dy := dy * g * DisplayResolution + cy;

      mstroke.x := Math.floor(dx);
      cx := dx - mstroke.x;
      mstroke.y := Math.floor(dy);
      cy := dy - mstroke.y;

      interception_send(context, device, @mstroke, 1);
    end;
  end;

  interception_destroy_context(context);
end;

end.
