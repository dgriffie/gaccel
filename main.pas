unit Main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  Menus, XMLPropStorage, ExtCtrls, ActnList, SpinEx, TAGraph, TASources,
  TASeries, TAFuncSeries, TATools, Types, simpleipc, driver, transfer,
  TAChartUtils, TACustomSeries, TACustomSource, TADrawUtils, Fileutil;

type

  { TForm1 }

  TForm1 = class(TForm)
    ActionSaveAs: TAction;
    ActionSave: TAction;
    ActionOpen: TAction;
    ActionList1: TActionList;
    Apply: TButton;
    Chart1: TChart;
    Chart1LineSeries1: TLineSeries;
    Chart1LineSeries2: TLineSeries;
    ChartToolset1: TChartToolset;
    ChartToolset1DataPointClickTool1: TDataPointClickTool;
    ChartToolset1DataPointDragTool1: TDataPointDragTool;
    ChartToolset1PanDragTool1: TPanDragTool;
    ChartToolset1UserDefinedTool1: TUserDefinedTool;
    ChartToolset1ZoomMouseWheelTool1: TZoomMouseWheelTool;
    CheckBoxEnabled: TCheckBox;
    EditPointerDPI: TEdit;
    EditPointerFrequency: TEdit;
    EditDisplayPPI: TEdit;
    LabelPointerDPI: TLabel;
    LabelPointerFrequency: TLabel;
    LabelDisplayPPI: TLabel;
    LabelOffset: TLabel;
    ListChartSource1: TListChartSource;
    MainMenu1: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItemDelete: TMenuItem;
    OpenDialog1: TOpenDialog;
    PopupMenu1: TPopupMenu;
    SaveDialog1: TSaveDialog;
    TrayIcon1: TTrayIcon;
    XMLPropStorage1: TXMLPropStorage;
    procedure ActionOpenExecute(Sender: TObject);
    procedure ActionSaveAsExecute(Sender: TObject);
    procedure ActionSaveExecute(Sender: TObject);
    procedure ApplyClick(Sender: TObject);
    procedure Chart1ExtentChanging(ASender: TChart);
    procedure Chart1LineSeries2CustomDrawPointer(ASender: TChartSeries;
      ADrawer: IChartDrawer; AIndex: integer; ACenter: TPoint);
    procedure ChartToolset1DataPointClickTool1PointClick(ATool: TChartTool;
      APoint: TPoint);
    procedure ChartToolset1DataPointClickTool2PointClick(ATool: TChartTool;
      APoint: TPoint);
    procedure ChartToolset1DataPointDragTool1Drag(ASender: TDataPointDragTool;
      var AGraphPoint: TDoublePoint);
    procedure ChartToolset1UserDefinedTool1AfterKeyDown(ATool: TChartTool;
      APoint: TPoint);
    procedure ChartToolset1UserDefinedTool1AfterKeyUp(ATool: TChartTool;
      APoint: TPoint);
    procedure ChartToolset1UserDefinedTool1AfterMouseUp(ATool: TChartTool;
      APoint: TPoint);
    procedure FormCreate(Sender: TObject);
    procedure FormWindowStateChange(Sender: TObject);
    procedure MenuItemDeleteClick(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);







  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

var
  Dr: TDriver;
  Tr: TLinearTransfer;

procedure TForm1.FormWindowStateChange(Sender: TObject);
begin
  if WindowState = wsMinimized then
    Hide;
end;

procedure TForm1.TrayIcon1Click(Sender: TObject);
begin
  WindowState := wsNormal;
  Show;
end;

procedure TForm1.MenuItemDeleteClick(Sender: TObject);
begin
  Chart1LineSeries1.Delete(ChartToolset1DataPointClickTool1.PointIndex);
end;

procedure TForm1.ApplyClick(Sender: TObject);
var
  i: integer;
begin
  if (Dr <> nil) then
  begin
    Dr.Terminate;
    Dr.Free;
    Dr := nil;
  end;
  if (Tr <> nil) then
  begin
    Tr.Free;
    Tr := nil;
  end;
  if CheckBoxEnabled.Checked then
  begin
    Tr := TLinearTransfer.Create;
    setlength(Tr.P, ListChartSource1.Count);
    i := 0;
    while i < ListChartSource1.Count do
    begin
      Tr.P[i].X := ListChartSource1.Item[i]^.X;
      Tr.P[i].Y := ListChartSource1.Item[i]^.Y;
      i := i + 1;
    end;

    Dr := TDriver.Create(False);
    Dr.PointerResolution := StrToFloat(EditPointerDPI.Text);
    Dr.PointerFrequency := StrToFloat(EditPointerFrequency.Text);
    Dr.DisplayResolution := StrToFloat(EditDisplayPPI.Text);
    Dr.Transfer := Tr;
    Dr.Start;
  end;

end;

procedure TForm1.ActionOpenExecute(Sender: TObject);
var
  f: string;
begin
  OpenDialog1.InitialDir := GetCurrentDir;
  if OpenDialog1.Execute then
  begin
    f := XMLPropStorage1.FileName;
    XMLPropStorage1.Save;
    XMLPropStorage1.FileName := OpenDialog1.FileName;
    try
      XMLPropStorage1.Restore;
    except
      XMLPropStorage1.FileName := f;
      Application.MessageBox('Didn''t work!', 'Error');
    end;
  end;
end;

procedure TForm1.ActionSaveAsExecute(Sender: TObject);
var
  f: string;
begin
  SaveDialog1.FileName := XMLPropStorage1.FileName;
  if SaveDialog1.Execute then
  begin
    f := XMLPropStorage1.FileName;
    try
      XMLPropStorage1.FileName := GetTempFileName;
      XMLPropStorage1.Save;
      CopyFile(XMLPropStorage1.FileName, SaveDialog1.FileName, False, True);
      XMLPropStorage1.FileName := SaveDialog1.FileName;
    except
      XMLPropStorage1.FileName:=f;
      Application.MessageBox('Didn''t work!', 'Error');
    end;
  end;
end;


procedure TForm1.ActionSaveExecute(Sender: TObject);
begin
  XMLPropStorage1.Save;
end;

procedure TForm1.ChartToolset1DataPointDragTool1Drag(ASender: TDataPointDragTool;
  var AGraphPoint: TDoublePoint);
var
  i: integer;
begin
  i := ASender.PointIndex;
  if (i > 0) and (AGraphPoint.X <= Chart1LineSeries1.GetXValue(i - 1)) then
    AGraphPoint.X := Chart1LineSeries1.GetXValue(i)
  else if AGraphPoint.X < 0 then
    AGraphPoint.X := 0
  else if (i < Chart1LineSeries1.LastValueIndex) and
    (AGraphPoint.X >= Chart1LineSeries1.GetXValue(i + 1)) then
    AGraphPoint.X := Chart1LineSeries1.GetXValue(i)
  else if AGraphPoint.X > Chart1.LogicalExtent.b.X then
    AGraphPoint.X := Chart1.LogicalExtent.b.X;

  if AGraphPoint.Y < 0 then
    AGraphPoint.y := 0
  else if AGraphPoint.Y > Chart1.LogicalExtent.b.Y then
    AGraphPoint.Y := Chart1.LogicalExtent.b.Y;
end;

procedure TForm1.ChartToolset1UserDefinedTool1AfterKeyDown(ATool: TChartTool;
  APoint: TPoint);
begin
  Chart1.Cursor := crCross;
end;

procedure TForm1.ChartToolset1UserDefinedTool1AfterKeyUp(ATool: TChartTool;
  APoint: TPoint);
begin
  Chart1.Cursor := crDefault;
end;

procedure TForm1.ChartToolset1UserDefinedTool1AfterMouseUp(ATool: TChartTool;
  APoint: TPoint);
var
  p: TDoublePoint;
begin
  p := Chart1.ImageToGraph(APoint);
  ;
  ListChartSource1.Add(p.X, p.Y, '', clRed);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin

end;

procedure TForm1.ChartToolset1DataPointClickTool1PointClick(ATool: TChartTool;
  APoint: TPoint);
begin
  if Chart1LineSeries1.Count < 3 then
    MenuItemDelete.Enabled := False
  else
    MenuItemDelete.Enabled := True;
  PopupMenu1.PopUp();
end;

procedure TForm1.ChartToolset1DataPointClickTool2PointClick(ATool: TChartTool;
  APoint: TPoint);
begin
  if Chart1LineSeries1.Count > 2 then
    Chart1LineSeries1.Delete(TDataPointClickTool(ATool).PointIndex);
end;

procedure TForm1.Chart1ExtentChanging(ASender: TChart);
var
  r: TDoubleRect;
begin
  r := Chart1.LogicalExtent;
  if r.a.X < 0 then
  begin
    r.b.X := r.b.X - r.a.X;
    r.a.X := 0;
  end;
  if r.a.Y < 0 then
  begin
    r.b.Y := r.b.Y - r.a.Y;
    r.a.Y := 0;
  end;

  Chart1.LogicalExtent := r;
end;

procedure TForm1.Chart1LineSeries2CustomDrawPointer(ASender: TChartSeries;
  ADrawer: IChartDrawer; AIndex: integer; ACenter: TPoint);
begin
  if AIndex = 0 then
  begin
    ADrawer.MoveTo(ACenter);
    ADrawer.LineTo(0, ACenter.Y);
  end
  else if AIndex = ASender.LastValueIndex then
  begin
    ADrawer.MoveTo(ACenter);
    ADrawer.LineTo(ASender.ParentChart.BoundsRect.Right, Acenter.Y);
  end;
end;

end.
