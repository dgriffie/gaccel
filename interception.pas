unit Interception;

{$mode objfpc}{$H+}

interface

uses ctypes;

const
  INTERCEPTION_MAX_KEYBOARD = 10;
  INTERCEPTION_MAX_MOUSE = 10;
  INTERCEPTION_MAX_DEVICE = INTERCEPTION_MAX_KEYBOARD + INTERCEPTION_MAX_MOUSE;

  INTERCEPTION_KEY_DOWN = $00;
  INTERCEPTION_KEY_UP = $01;
  INTERCEPTION_KEY_E0 = $02;
  INTERCEPTION_KEY_E1 = $04;
  INTERCEPTION_KEY_TERMSRV_SET_LED = $08;
  INTERCEPTION_KEY_TERMSRV_SHADOW = $10;
  INTERCEPTION_KEY_TERMSRV_VKPACKET = $20;

  INTERCEPTION_FILTER_KEY_NONE = $0000;
  INTERCEPTION_FILTER_KEY_DOWN = $0001;
  INTERCEPTION_FILTER_KEY_UP = $0010;
  INTERCEPTION_FILTER_KEY_E0 = $0020;
  INTERCEPTION_FILTER_KEY_E1 = $0040;
  INTERCEPTION_FILTER_KEY_TERMSRV_SET_LEF = $0080;
  INTERCEPTION_FILTER_KEY_TERMSRV_SHADOW = $0100;
  INTERCEPTION_FILTER_KEY_TERMSRV_VKPACKET = $0200;
  INTERCEPTION_FILTER_KEY_ALL = $FFFF;

  INTERCEPTION_MOUSE_BUTTON_1_DOWN = $001;
  INTERCEPTION_MOUSE_BUTTON_1_UP = $002;
  INTERCEPTION_MOUSE_BUTTON_2_DOWN = $004;
  INTERCEPTION_MOUSE_BUTTON_2_UP = $008;
  INTERCEPTION_MOUSE_BUTTON_3_DOWN = $010;
  INTERCEPTION_MOUSE_BUTTON_3_UP = $020;
  INTERCEPTION_MOUSE_BUTTON_4_DOWN = $040;
  INTERCEPTION_MOUSE_BUTTON_4_UP = $080;
  INTERCEPTION_MOUSE_BUTTON_5_DOWN = $100;
  INTERCEPTION_MOUSE_BUTTON_5_UP = $200;
  INTERCEPTION_MOUSE_WHEEL = $400;
  INTERCEPTION_MOUSE_HWHEEL = $800;

  INTERCEPTION_MOUSE_LEFT_BUTTON_DOWN = $001;
  INTERCEPTION_MOUSE_LEFT_BUTTON_UP = $002;
  INTERCEPTION_MOUSE_RIGHT_BUTTON_DOWN = $004;
  INTERCEPTION_MOUSE_RIGHT_BUTTON_UP = $008;
  INTERCEPTION_MOUSE_MIDDLE_BUTTON_DOWN = $010;
  INTERCEPTION_MOUSE_MIDDLE_BUTTON_UP = $020;

  INTERCEPTION_FILTER_MOUSE_NONE = $0000;
  INTERCEPTION_FILTER_MOUSE_BUTTON_1_DOWN = $0001;
  INTERCEPTION_FILTER_MOUSE_BUTTON_1_UP = $0002;
  INTERCEPTION_FILTER_MOUSE_BUTTON_2_DOWN = $0004;
  INTERCEPTION_FILTER_MOUSE_BUTTON_2_UP = $0008;
  INTERCEPTION_FILTER_MOUSE_BUTTON_3_DOWN = $0010;
  INTERCEPTION_FILTER_MOUSE_BUTTON_3_UP = $0020;
  INTERCEPTION_FILTER_MOUSE_BUTTON_4_DOWN = $0040;
  INTERCEPTION_FILTER_MOUSE_BUTTON_4_UP = $0080;
  INTERCEPTION_FILTER_MOUSE_BUTTON_5_DOWN = $0100;
  INTERCEPTION_FILTER_MOUSE_BUTTON_5_UP = $0200;
  INTERCEPTION_FILTER_MOUSE_WHEEL = $0400;
  INTERCEPTION_FILTER_MOUSE_HWHEEL = $0800;
  INTERCEPTION_FILTER_MOUSE_MOVE = $1000;
  INTERCEPTION_FILTER_MOUSE_ALL = $FFFF;

  INTERCEPTION_FILTER_MOUSE_LEFT_BUTTON_DOWN = $0001;
  INTERCEPTION_FILTER_MOUSE_LEFT_BUTTON_UP = $0002;
  INTERCEPTION_FILTER_MOUSE_RIGHT_BUTTON_DOWN = $0004;
  INTERCEPTION_FILTER_MOUSE_RIGHT_BUTTON_UP = $0008;
  INTERCEPTION_FILTER_MOUSE_MIDDLE_BUTTON_DOWN = $0010;
  INTERCEPTION_FILTER_MOUSE_MIDDLE_BUTTON_UP = $0020;

  INTERCEPTION_MOUSE_MOVE_RELATIVE = $000;
  INTERCEPTION_MOUSE_MOVE_ABSOLUTE = $001;
  INTERCEPTION_MOUSE_VIRTUAL_DESKTOP = $002;
  INTERCEPTION_MOUSE_ATTRIBUTES_CHANGED = $004;
  INTERCEPTION_MOUSE_MOVE_NOCOALESCE = $008;
  INTERCEPTION_MOUSE_TERMSRV_SRC_SHADOW = $0100;

type
  InterceptionContext = pointer;
  InterceptionDevice = cint;
  InterceptionPrecedence = cint;
  InterceptionFilter = cushort;
  InterceptionPredicate = function(device: InterceptionDevice): cint;

  InterceptionMouseStroke = record
    state: cushort;
    flags: cushort;
    rolling: cshort;
    x: cint;
    y: cint;
    information: cuint;
  end;

  InterceptionKeyStroke = record
    code: cushort;
    state: cushort;
    information: cuint;
  end;

function interception_create_context(): InterceptionContext;
  external 'interception.dll';

procedure interception_destroy_context(context: InterceptionContext);
  external 'interception.dll';

function interception_get_precedence(context: InterceptionContext;
  device: InterceptionDevice): InterceptionPrecedence; external 'interception.dll';

procedure interception_set_precedence(context: InterceptionContext;
  device: InterceptionDevice; precedence: InterceptionPrecedence);
  external 'interception.dll';

function interception_get_filter(context: InterceptionContext;
  device: InterceptionDevice): InterceptionFIlter; external 'interception.dll';

procedure interception_set_filter(context: InterceptionContext;
  predicate: InterceptionPredicate; filter: InterceptionFilter);
  external 'interception.dll';

function interception_wait(context: InterceptionContext): InterceptionDevice;
  external 'interception.dll';

function interception_wait_with_timeout(context: InterceptionContext;
  milliseconds: dword): InterceptionDevice; external 'interception.dll';

function interception_send(context: InterceptionContext;
  device: InterceptionDevice; stroke: Pointer; nstroke: cuint): cint;
  external 'interception.dll';

function interception_receive(context: InterceptionContext;
  device: InterceptionDevice; stroke: Pointer; nstroke: cuint): cint;
  external 'interception.dll';

function interception_get_hardware_id(context: InterceptionContext;
  device: InterceptionDevice; hardware_id_buffer: Pointer; buffer_size: cuint): cuint;
  external 'interception.dll';

function interception_is_invalid(device: InterceptionDevice): cint;
  external 'interception.dll';

function interception_is_mouse(device: InterceptionDevice): cint;
  external 'interception.dll';


implementation

end.
