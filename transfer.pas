unit Transfer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Types;

type
  TTransfer = class
    function Gain(v: double): double; virtual; abstract;
  end;

  TDoublePoint = record
    X, Y: double;
  end;

  TLinearTransfer = class(TTransfer)
    P: array of TDoublePoint;
    function Gain(v: double): double; override;
  end;

implementation

function TLinearTransfer.Gain(V: double): double;
var
  i: integer = 0;
begin
  while (i < length(P)) and (V > P[i].X) do
    i := i + 1;
  if i = 0 then
    Result := P[0].Y
  else if i = length(P) then
    Result := P[i - 1].Y
  else
    Result := P[i-1].Y + (P[i].Y - P[i - 1].Y) * (V - P[i - 1].X) / (P[i].X - P[i - 1].X);
end;


end.
